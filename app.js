var express = require('express');
var path = require('path');
var logger = require('morgan');
var expressSession = require('express-session');
var bodyParser = require('body-parser');
var config = require('./config')();
var passport = require('./config/passport');
var fs = require('fs');

var routes = require('./routes/index');
var admin = require('./routes/admin');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false, limit: 20 * 1000000, parameterLimit: 1000000}));
app.use(require('skipper')());
app.use(expressSession({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: { httpOnly: true, secure: false }
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/admin/', admin);
app.get('/ckeditor/pictures', function (req, res) {
  fs.readdir(path.join(__dirname + '/public/images'), function (err, files) {
    files = files.map(function (file) {
      return path.join('/images/' + file);
    });
    var paired = [];
    files.forEach(function (element, index) {
      if (index % 4 === 0) {
        paired.push([element])
      } else {
        paired[paired.length - 1].push(element)
      }
    });
    res.render('admin/pictures', {
      CKEditorFuncNum: req.query.CKEditorFuncNum,
      files: paired
    });
  });
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;

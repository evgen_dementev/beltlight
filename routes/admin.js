var express = require('express');
var db = require('diskdb');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var utils = require('../middleware/utils');
var path = require('path');
var fs = require('fs');
var _ = require('lodash');
var router = express.Router();

router.get('/login', function (req, res) {
  res.render('admin/login');
});

router.post('/login', passport.authenticate('local',
  {
    successRedirect: '/admin',
    failureRedirect: '/admin'
  })
);

router.post('/signup', function (req, res) {
  var salt = bcrypt.genSaltSync();
  var hashPassword = bcrypt.hashSync(req.body.password, salt);

  db.users.save(
    {
      email: req.body.email,
      password: hashPassword
    }
  );

  res.json({ status: 'ok' });
});

router.get('/', utils.isLogin, function (req, res) {
  var mainText = db.text.findOne({
    type: 'main'
  });
  var saleText = db.text.findOne({
    type: 'sale'
  });
  var setText = db.text.findOne({
    type: 'set'
  });
  var lendText = db.text.findOne({
    type: 'lend'
  });
  var badge = db.feedbacks.find({ seen: false }).length;

  res.render('admin/main', {
    badge: badge,
    user: req.user,
    mainText: _.get(mainText, 'text', '').replace(/(\r\n|\n|\r)/gm, ""),
    saleText: _.get(saleText, 'text', '').replace(/(\r\n|\n|\r)/gm, ""),
    lendText: _.get(lendText, 'text', '').replace(/(\r\n|\n|\r)/gm, ""),
    setText: _.get(setText, 'text', '').replace(/(\r\n|\n|\r)/gm, "")
  });
});

router.get('/mainSlider', utils.isLogin, function (req, res) {
  var badge = db.feedbacks.find({ seen: false }).length;
  var slider = fs.readdirSync('./public/images/constImages/slider');

  slider = _.compact(_.map(slider, function (slide) {
    if (slide !== 'gitkeep')
      return {
        _id: slide,
        path: '/images/constImages/slider/' + slide
      }
  }));

  res.render('admin/main/slider', {
    badge: badge,
    user: req.user,
    slider: slider || []
  });
});

router.post('/mainSlider/create', utils.isLogin, function (req, res) {
  req.file('slide').upload({dirname: path.resolve(__dirname, '../public/images/constImages/slider')}, function (err, files) {
    res.json({});
  });
});

router.delete('/mainSlider/:id', utils.isLogin, function (req, res) {
  var id = req.params.id;

  fs.unlink(path.resolve(__dirname, '../public/images/constImages/slider/', id), function () {
    res.json({});
  });
});

router.get('/items', utils.isLogin, function (req, res) {
  var items = db.items.find();
  var badge = db.feedbacks.find({ seen: false }).length;
  var paired = [];

  items = _.sortBy(items, 'order');
  items.forEach(function (element, index) {
    if (index % 4 === 0) {
      paired.push([element])
    } else {
      paired[paired.length - 1].push(element)
    }
  });

  res.render('admin/main/items',
    {
      badge: badge,
      user: req.user,
      items: paired || []
    }
  );
});

router.get('/item/create', utils.isLogin, function (req, res) {
  res.render('admin/main/itemCreate',
    {
      user: req.user,
      item: {}
    }
  );
});

router.get('/item/edit/:id', utils.isLogin, function (req, res) {
  var id = req.params.id;
  var item = db.items.findOne({ _id: id });
  var badge = db.feedbacks.find({ seen: false }).length;

  item.attr = _.get(item, 'attr', '').replace(/(\r\n|\n|\r)/gm, "");
  item.desc = _.get(item, 'desc', '').replace(/(\r\n|\n|\r)/gm, "");
  item.price = _.get(item, 'price', '').replace(/(\r\n|\n|\r)/gm, "");
  res.render('admin/main/itemCreate',
    {
      badge: badge,
      user: req.user,
      item: item
    }
  );
});

router.post('/item/create', utils.isLogin, function (req, res) {
  var id = req.body.id;
  var name = req.body.name;
  var order = req.body.order;
  var attr = req.body.attr;
  var desc = req.body.desc;
  var price = req.body.price;

  req.file('photo').upload({ dirname: path.resolve(__dirname, '../public/images/') },
    function (err, files) {
      var photos = _.map(files, function (file) {
        return path.join('/images/' + file.fd.substr(file.fd.lastIndexOf('/')));
      });

      if (id) {
        var item = db.items.findOne({ _id: id });
        db.items.update(
          {
            _id: id
          },
          {
            name: name,
            order: order,
            attr: attr,
            desc: desc,
            price: price,
            photos: _.concat(photos, item.photos)
          });
      } else {
        db.items.save(
          {
            name: name,
            order: order,
            attr: attr,
            desc: desc,
            price: price,
            photos: photos
          }
        );
      }

      res.json({});
    });
});

router.delete('/item/delete/:id', utils.isLogin, function (req, res) {
  var item = db.items.findOne({ _id: req.params.id });

  item.photos.forEach(function (elem) {
    fs.unlink(path.resolve('./public/' + elem));
  });
  db.items.remove({ _id: req.params.id });

  res.json({});
});

router.put('/item/:id/photo', utils.isLogin, function (req, res) {
  var id = req.params.id;
  var photoId = req.body.photoId;
  var item = db.items.findOne({ _id: id });

  item.photos = _.pull(item.photos, photoId);
  db.items.update(
    {
      _id: id
    },
    {
      name: item.name,
      order: item.order,
      attr: item.attr,
      desc: item.desc,
      price:item.price,
      photos: item.photos
    }
  );
  fs.unlink(path.resolve('./public/' + photoId));

  res.json({});
});

router.get('/feedbacks', utils.isLogin, function (req, res) {
  var feedbacks = db.feedbacks.find();
  var badge = db.feedbacks.find({ seen: false }).length;
  feedbacks = _.sortBy(feedbacks, 'sort');

  res.render('admin/main/feedbacks',
    {
      badge: badge,
      user: req.user,
      feedbacks: feedbacks
    }
  );
});

router.get('/feedbacks/:id/delete', utils.isLogin, function (req, res) {
  var id = req.params.id;
  db.feedbacks.remove({ _id: id });
  var feedbacks = db.feedbacks.find();
  var badge = db.feedbacks.find({ seen: false }).length;
  feedbacks = _.sortBy(feedbacks, 'sort');

  res.render('admin/main/feedbacks',
    {
      badge: badge,
      user: req.user,
      feedbacks: feedbacks
    }
  );
});

router.get('/feedbacks/:id',  utils.isLogin, function (req, res) {
  var id = req.params.id;
  var feedback = db.feedbacks.findOne({ _id: id });

  db.feedbacks.update(
    {
      _id: id
    },
    {
      name: feedback.name,
      email: feedback.email,
      telephone: feedback.telephone,
      message: feedback.message,
      sort: feedback.sort,
      date: feedback.date,
      seen: true
    }
  );

  var badge = db.feedbacks.find({ seen: false }).length;

  res.render('admin/main/feedback',
    {
      badge: badge,
      user: req.user,
      feedback: feedback
    }
  );
});

router.post('/feedback', function (req, res) {
  db.feedbacks.save(
    {
      name: req.body.name,
      email: req.body.email,
      telephone: req.body.telephone,
      message: req.body.text,
      seen: false,
      date: new Date().toLocaleString(),
      sort: _.now()
    }
  );

  res.json({ status: 'ok' });
});

router.post('/ckeditor/pictures', utils.isLogin, function (req, res) {
  req.file('upload').upload({ dirname: path.resolve(__dirname, '../public/images/') }, function (err, uploadedFiles) {
    if (err) return res.send(500, err);
    return res.json('Файл загружен' + uploadedFiles[0].filename);
  });
});

router.post('/ckeditor/text', utils.isLogin, function (req, res) {
  var type = req.query.type;
  var text = db.text.findOne({ type: type });

  if (text) {
    db.text.update(
      {
        _id: text._id
      },
      {
        text: req.body.text
      }
    );
  } else {
    db.text.save(
      {
        text: req.body.text,
        type: type
      }
    );
  }

  res.json({});
});

router.get('/trash', utils.isLogin, function (req, res) {
  var itemsPhotos = _.filter(db.items.find(), function(elem) {
    return
  });
  var text = db.text.find();
  var files = fs.readdir(path.resolve(__dirname + '/../public/images'), function(err, files) {
    console.log(files);
  });

  console.log(files)
});

module.exports = router;

var express = require('express');
var router = express.Router();
var db = require('diskdb');
var _ = require('lodash');
var fs = require('fs');
var path = require('path');

/* GET home page. */
router.get('/', function (req, res) {
  var mainText = db.text.findOne({
    type: 'main'
  });
  var saleText = db.text.findOne({
    type: 'sale'
  });
  var setText = db.text.findOne({
    type: 'set'
  });
  var lendText = db.text.findOne({
    type: 'lend'
  });
  var items = db.items.find();
  var slider = fs.readdirSync('./public/images/constImages/slider');
  slider = _.compact(_.map(slider, function (slide) {
    if (slide != 'gitkeep')
      return 'images/constImages/slider/' + slide;
  }));
  items = _.sortBy(items, 'order');

  res.render('index', {
    user: req.user,
    slider: slider,
    mainText: _.get(mainText, 'text', '').replace(/(\r\n|\n|\r)/gm, ""),
    saleText: _.get(saleText, 'text', '').replace(/(\r\n|\n|\r)/gm, ""),
    lendText: _.get(lendText, 'text', '').replace(/(\r\n|\n|\r)/gm, ""),
    setText: _.get(setText, 'text', '').replace(/(\r\n|\n|\r)/gm, ""),
    items: items || []
  });
});

module.exports = router;

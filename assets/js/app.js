$('document').ready(function () {
  $(".anchor li a").on('click', function (e) {
    // prevent default anchor click behavior
    e.preventDefault();
    // store hash
    var hash = this.hash;

    // animate
    $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 300)
  });

    var jssor_1_SlideshowTransitions = [
      {$Duration:1200,$Opacity:2}
    ];

    var jssor_1_options = {
      $AutoPlay: true,
      $SlideshowOptions: {
        $Class: $JssorSlideshowRunner$,
        $Transitions: jssor_1_SlideshowTransitions,
        $TransitionsOrder: 1
      },
      $ArrowNavigatorOptions: {
        $Class: $JssorArrowNavigator$
      },
      $BulletNavigatorOptions: {
        $Class: $JssorBulletNavigator$
      }
    };

    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

    function ScaleSlider() {
      var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
      if (refSize) {
        refSize = Math.min(refSize, 1170);
        jssor_1_slider.$ScaleWidth(refSize);
      }
      else {
        window.setTimeout(ScaleSlider, 30);
      }
    }
    ScaleSlider();
    $(window).bind("load", ScaleSlider);
    $(window).bind("resize", ScaleSlider);
    $(window).bind("orientationchange", ScaleSlider);
    //responsive code end

  $('div[id^="carouselLend-"]').waterwheelCarousel();
  $('div[id^="carousel-"]').waterwheelCarousel();

  function validate() {
      if ($(this).val() === '') {
        $(this).parent().removeClass('has-success');
        $(this).parent().addClass('has-error');
      } else {
        $(this).parent().removeClass('has-error');
        $(this).parent().addClass('has-success');
      }
  }

  $('#inputName').on('keydown', validate);
  $('#inputTelephone').on('keydown', validate);
  $('#inputText').on('keydown', validate);

  $('#feedback button').click(function (e) {
    e.preventDefault();
    var data = new FormData();
    var name = $('#inputName').val();
    var email = $('#inputEmail3').val();
    var telephone = $('#inputTelephone').val();
    var text = $('#inputText').val();

    if (!text)
      $('#inputText').parent().addClass('has-error');
    if (!telephone)
      $('#inputTelephone').parent().addClass('has-error');
    if (!name)
      $('#inputName').parent().addClass('has-error');

    if (text && telephone && name) {
      data.append('name', name);
      data.append('email', email);
      data.append('telephone', telephone);
      data.append('text', text);
      $.ajax({
        type: "POST",
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        url: "/admin/feedback",
        success: function (data) {
          $('#myModal').modal('show');
          $('#feedback')[0].reset();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.error(textStatus, errorThrown)
        }
      });
    }
  });
});


var gulp = require('gulp');
var gulpSass = require('gulp-sass');
var minify = require('gulp-minify');

gulp.task('sass', function () {
  return gulp.src('./assets/stylesheet/*.scss')
    .pipe(gulpSass().on('error', gulpSass.logError))
    .pipe(gulp.dest('./public/stylesheet'));
});

gulp.task('fonts', function () {
  return gulp.src('./assets/fonts/**/*')
    .pipe(gulp.dest('./public/fonts'));
});

gulp.task('js', function () {
  return gulp.src([
      './assets/js/*',
      'bower_components/jquery/dist/jquery.min.js',
      'bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js',
      'bower_components/waterwheelcarousel/js/jquery.waterwheelCarousel.js',
      'bower_components/jssor-slider/js/jssor.slider.mini.js'
    ])
    .pipe(gulp.dest('./public/javascripts'));
});

gulp.task('default', function () {
  gulp.watch('./assets/stylesheet/*.scss', ['sass', 'fonts']);
  gulp.watch('./assets/js/*', ['js']);
});

gulp.task('build', ['sass', 'fonts', 'js']);
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var db = require('diskdb');
var bcrypt = require('bcrypt-nodejs');

passport.serializeUser(function (user, done) {
  done(null, user._id);
});

passport.deserializeUser(function (id, done) {
  var user = db.users.findOne({ _id: id });
  done(false, user);
});

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true,
    session: true
  },
  function (req, email, password, done) {
    var user = db.users.findOne({ email: email });
    if (!user) {
      return done(null, false);
    }
    if (!bcrypt.compareSync(password, user.password)) {
      return done(null, false);
    }
    return done(null, user);
  }
));

module.exports = passport;
module.exports = {
  isLogin: function (req, res, next) {
    if (!req.user) res.redirect('/admin/login');
    else next();
  }
};